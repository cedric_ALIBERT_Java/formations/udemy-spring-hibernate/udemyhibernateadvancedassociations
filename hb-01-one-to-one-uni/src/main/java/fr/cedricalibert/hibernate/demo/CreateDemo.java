package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;

public class CreateDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//create the objects
			//Instructor instructor = new Instructor("Cédric", "ALIBERT", "test@test.fr");
			//InstructorDetail instructorDetail = new InstructorDetail("Tets", "Photography");
			
			//save an other
			Instructor instructor = new Instructor("Cédric2", "ALIBERT2", "test2@test.fr");
			InstructorDetail instructorDetail = new InstructorDetail("Tets2", "Code");
			
			//associate the objetcs together
			instructor.setInstructorDetail(instructorDetail);
			
			//start a transaction
			session.beginTransaction();
			
			//save the instructor
			//because cascade type, save also the instructor detail
			System.out.println("Saving instructor : "+instructor);
			session.save(instructor);
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		finally {
			factory.close();
		}

	}

}
