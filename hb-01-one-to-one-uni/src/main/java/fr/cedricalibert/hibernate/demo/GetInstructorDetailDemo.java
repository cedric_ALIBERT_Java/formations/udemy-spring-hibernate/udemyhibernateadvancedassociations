package fr.cedricalibert.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;

public class GetInstructorDetailDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			//get instrucotr detail object
			int id=2999;
			InstructorDetail instructorDetail = session.get(InstructorDetail.class, id);
			
			//print instructor detail
			System.out.println("Instructor detail : "+instructorDetail);
			
			//print associated instructor
			System.out.println("Associate Instructor: "+instructorDetail.getInstructor());
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
